import api.ApiInterface;
import api.ApiRequest;
import data.DataSource;
import helper.FindCity;
import model.BaseModel;
import model.Weather;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.Scanner;

public class Assignment2 {
    public static void main(String args[]) {
        String appId="c4eebfe4a41bd0d73cc34ecb62c8a8b0";
        Scanner input = new Scanner(System.in);
        System.out.print("Input a city name: ");
        String city = input.next();
        if(city!=null){
            JSONArray arr= DataSource.getDataSource();
            JSONObject response = FindCity.getCityData(arr,city);
            JSONObject coordinats=(JSONObject) response.get("coord");
            System.out.println("Latitude "+coordinats.get("lat")+" "+"Longitude "+coordinats.get("lon"));
            System.out.println("Country "+response.get("country"));

            ApiInterface apiInterface = ApiRequest.getApiService();
            Call<BaseModel> baseModelCall = apiInterface.getTemp(city,appId);
            baseModelCall.enqueue(new Callback<BaseModel>() {
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    if(response.isSuccessful()){
                        Double temp=response.body().getMain().getTemp();
                        Weather weather=response.body().getWeather().get(0);
                        System.out.println("Weather is "+weather.getDescription());
                        System.out.println("Temperature is "+temp);
                    }
                }

                public void onFailure(Call<BaseModel> call, Throwable throwable) {
                    System.out.println("Temperature could not fetch.");
                }
            });
        }

    }
}

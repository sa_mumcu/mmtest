package data;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class DataSource {
    public static JSONArray getDataSource(){
        JSONArray arr = null;
        try {
            arr = (JSONArray) new JSONParser().parse(new FileReader("city.list.json"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return arr;
    }
}

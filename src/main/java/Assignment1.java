import data.DataSource;
import helper.FindCity;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Scanner;

public class Assignment1 {
    public static void main(String args[]) {

        Scanner input = new Scanner(System.in);
        System.out.print("Input a city name: ");
        String city = input.next();
        if(city!=null){
            JSONArray arr= DataSource.getDataSource();
            JSONObject response =FindCity.getCityData(arr,city);
            JSONObject coordinats=(JSONObject) response.get("coord");
            System.out.println("Latitude "+coordinats.get("lat")+" "+"Longitude "+coordinats.get("lon"));
            System.out.println("Country "+response.get("country"));
        }
    }
}

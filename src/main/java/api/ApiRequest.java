package api;

import api.ApiInterface;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

public class ApiRequest {

    public static String BASEURL="https://api.openweathermap.org/data/2.5/";

    private static Retrofit getRefrofitInstance(){
        Retrofit rf = new Retrofit.Builder()
                .baseUrl(BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return rf;
    }

    public static ApiInterface getApiService(){
        ApiInterface api = getRefrofitInstance().create(ApiInterface.class);
        return api;
    }
}

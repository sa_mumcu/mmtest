package api;

import model.BaseModel;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("weather")
    Call<BaseModel> getTemp(
            @Query("q") String city,
            @Query("appid") String appId);

}

package helper;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Iterator;

public class FindCity {
    public static JSONObject getCityData(JSONArray arr,String city){
        Iterator itr2 = arr.iterator();
        String city3=null;
        JSONObject response=null;

        while (itr2.hasNext())
        {
            JSONObject jo= (JSONObject) itr2.next();
            if(jo.get("name").toString().equals(city)){
                city3=jo.get("name").toString();
                response=(JSONObject) jo;
                break;
            }
        }
        return response;
    }
}

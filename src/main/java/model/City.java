package model;

public class City {
    private Integer id;
    private String name;
    private String country;
    private Coordinats coordinats;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Coordinats getCoordinats() {
        return coordinats;
    }

    public void setCoordinats(Coordinats coordinats) {
        this.coordinats = coordinats;
    }
}

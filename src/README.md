### Details About Test
There are two runnable files for the task.

Assignment1 java files does the first assignment and Assignment2 does the second assignment.

**In Assignment1** we are taking city as an input then we are using local data source to fetch city's gps and country data.

**In Assignment2** we are again taking city as an input and then we are fetching data about city.
After we have the city name we are using remote data source via api calls.
I used openweathermap api for getting weather information about city.
You should get an api key from this site to make an api request. https://openweathermap.org/api
After you get your own api key you can change the appId string data in Assignment2 class.

I used singleton design pattern for accessing remote data with weather api.

You can email me any suggestion or if I made something wrong.